/* eslint-disable no-unused-vars */
import MagnifyingGlass from './magnifying-glass';
import Sortable from './sortable';
import SVG from './svg';
import SVGRadialMenu from './svg-lib/svg-radial-menu';
import Editor from './editor/editor';
import Keywords from './keywords';
import TestQuiz from './test-quiz';
import FullscreenToggle from './fullscreen';
import UserText from './user-text';
import AudioPlayerNormal from './media/audio-player-normal';
import AudioPlayerSimple from './media/audio-player-simple';
import FakeMap from './fake-map';
import { stopPlayingVideoPlayers } from './media/media';
import VideoPlayer from './media/video';
import Selectable from './selectable';
import NewTable from './new-table';
import Draggable from './draggable';
import Gallery from './gallery';

const windowLoadComponents = () => {
  console.log('windowLoad:components');

  // Video player - normal
  const $videoPlayers = document.querySelectorAll('[data-player-video]');
  if ($videoPlayers.length > 0) {
    $videoPlayers.forEach(($videoPlayer) => {
      // console.log('$videoPlayer', $videoPlayer);
      const videoPlayer = new VideoPlayer($videoPlayer);
    });

    document.addEventListener('slide.change', () => {
      stopPlayingVideoPlayers($videoPlayers);
    });
  }

  // Audio player - normal
  const $audioPlayerNormals = document.querySelectorAll('[data-player-audio="normal"]');
  if ($audioPlayerNormals.length > 0) {
    $audioPlayerNormals.forEach(($audioPlayerNormal) => {
      const audioPlayerNormal = new AudioPlayerNormal($audioPlayerNormal);
    });

    // TODO > audio-player-normal.js#61
    // document.addEventListener('slide.change', () => {
    //   stopPlayingVideoPlayers($audioPlayerNormals);
    // });
  }

  // Audio player — simple
  const $audioPlayerSimples = document.querySelectorAll('[data-player-audio="simple"]');
  if ($audioPlayerSimples.length > 0) {
    $audioPlayerSimples.forEach(($audioPlayerSimple) => {
      const audioPlayerSimple = new AudioPlayerSimple($audioPlayerSimple);
    });

    document.addEventListener('slide.change', () => {
      stopPlayingVideoPlayers($audioPlayerSimples);
    });
  }

  // Fullscreen
  const $fullscreenToggles = document.querySelectorAll('[data-fullscreen="toggle"]');
  if ($fullscreenToggles.length > 0) {
    $fullscreenToggles.forEach(($fullscreenToggle) => {
      const fullscreenToggle = new FullscreenToggle($fullscreenToggle);
    });
  }

  // Magnifying glass
  const $magnifyingGlass = document.querySelectorAll('[data-lupa-target]');
  if ($magnifyingGlass.length > 0) {
    for (let i = 0; i < $magnifyingGlass.length; i += 1) {
      const magnifyingGlass = new MagnifyingGlass($magnifyingGlass[i]);
    }
  }

  // Fake Map
  const $fakeMaps = document.querySelectorAll('[data-fake-map="target"]');
  if ($fakeMaps.length > 0) {
    for (let i = 0; i < $fakeMaps.length; i += 1) {
      const fakeMap = new FakeMap($fakeMaps[i]);
    }
  }

  //
  // load these below in working mode only

  // User texts
  // - preview/working mode logic inside the component
  //  - show
  const $userTexts = document.querySelectorAll('[data-textarea-target=""]');
  if ($userTexts.length > 0) {
    $userTexts.forEach(($userText) => {
      const userText = new UserText($userText);
    });
  }

  // SVG + Radial menu
  // - preview/working mode logic in side the component
  //  - to set max-width
  const $svgs = document.querySelectorAll('[data-svg-target]');
  if ($svgs.length > 0) {

    if (HISTORYLAB.import.done !== true) {
      // radial menu
      const $radialMenu = document.querySelector('[data-radial-menu]');
      if ($radialMenu) {
        const radialMenu = new SVGRadialMenu($radialMenu, $svgs);
      }
    }

    // svgs
    // setSize is needed
    $svgs.forEach(($svg) => {
      const svg = new SVG($svg);
    });
  }

  if (HISTORYLAB.import.done !== true) {

    // Editor
    const $editors = document.querySelectorAll('[data-editor]');
    if ($editors.length > 0) {
      $editors.forEach(($editor) => {
        const editor = new Editor($editor);
      });
    }

    // Test Quizes
    const $testQuizs = document.querySelectorAll('[data-target-test-quiz]');
    if ($testQuizs.length > 0) {
      $testQuizs.forEach(($testQuiz) => {
        const settings = document.querySelector(
          `[data-test-quiz-id-reference="${$testQuiz.getAttribute(
            'data-test-quiz-id',
          )}"]`,
        );

        const testQuiz = new TestQuiz(
          $testQuiz.getAttribute('data-test-quiz-id'),
          $testQuiz,
          JSON.parse(settings.getAttribute('data-module-settings')),
        );
        testQuiz.init();
      });
    }

    // New table
    const $newTables = document.querySelectorAll('[data-new-table]');
    if ($newTables.length > 0) {
      $newTables.forEach(($newTable) => {
        const newTable = new NewTable($newTable);
      });
    }

    // Pretahovani
    const $draggables = document.querySelectorAll('[data-draggable]');
    if ($draggables.length > 0) {
      $draggables.forEach(($draggable) => {
        const draggable = new Draggable($draggable);
      });
    }

    // Sortable
    const $sortables = document.querySelectorAll('[data-sortable-target]');
    if ($sortables.length > 0) {
      $sortables.forEach(($sortable) => {
        const sortable = new Sortable($sortable);
      });
    }

    // Selectable
    const $selectables = document.querySelectorAll('[data-selectable-target]');
    if ($selectables.length > 0) {
      $selectables.forEach(($selectable) => {
        const selectable = new Selectable($selectable);
      });
    }

    // Keywords selection
    const $keywords = document.querySelectorAll('[data-keywords]');
    if ($keywords.length > 0) {
      $keywords.forEach(($keyword) => {
        const settings = $keyword.hasAttribute('data-module-settings')
          ? $keyword.getAttribute('data-module-settings')
          : {};
        const keywords = new Keywords($keyword, JSON.parse(settings));
        keywords.init();
      });
    }

    // Gallery listener
    const $galleries = document.querySelectorAll('[data-gallery]');
    if ($galleries.length > 0) {
      $galleries.forEach(($gallery) => {
        const gallery = new Gallery($gallery);
      });
    }
  }
};

export default windowLoadComponents;

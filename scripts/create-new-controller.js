// npm run new-controller -- --slug cviceni-test-1
// notice the double dash "--", see: https://docs.npmjs.com/cli/run-script

const fs = require('fs');

// config
const config = {
  folderSettings: {
    permissions: 0744,
  },
  path: {
    folder: 'src/scaffolding-templates/controller/',
    controllerTemplate: {
      folder: 'src/scaffolding-templates/controller/',
    },
    controllerSrcFolder: {
      folder: 'src/js/controllers/'
    },
    js: {
      folder: 'src/js/',
      controllerLoader: 'src/js/cviceni/controller-load.js'
    }
  },
  placeholders: {
    controllerName: '{REPLACE_CONTROLLER_NAME}',
  }
};


function upperCaseFirst(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

// create JSON data file name from --slug to the camelCase:
// 'name-of-cviceni' –> 'cviceniNameOfCviceni'
function createControllerName(str) {
  let stringArray = str.split('-');

  stringArray.forEach((part, index) => {
    stringArray[index] = upperCaseFirst(part);
  });

  return 'controller' + stringArray.join('');
}


// command line arguments order
const slugIndex = process.argv.indexOf('--slug');


let slug;
// command line arguments values
if (slugIndex > -1) {
  slug = process.argv[slugIndex + 1];
}


// data file name
const controllerName = createControllerName(slug);
// pug folder name
const controllerSrcFolder = `${config.path.controllerSrcFolder.folder}${slug}/`;


// create new folder
// fs.mkdir(path[, mode], callback)
fs.mkdir(controllerSrcFolder, config.folderSettings.permissions, (error) => {

  if (error) {
    console.log(error);

    if (error.code == 'EEXIST') { }
  }

  // scaffold
  // fs.readFile(path[, options], callback)
  fs.readFile(config.path.controllerTemplate.folder + 'controller.js', 'utf8', (error, data) => {

    if (error) {
      return console.log(error);
    }

    console.log("data: ", data);

    // replace variable name
    let result = data.replace(new RegExp(config.placeholders.controllerName, 'g'), controllerName);


    // copy the file to the cviceni folder
    fs.writeFile(controllerSrcFolder + 'controller.js', result, 'utf8', (error) => {

      if (error) {
        return console.log(error);
      }

    });
  });

});

// add controller import to controller-load.js
fs.readFile(config.path.js.controllerLoader, 'utf8', (error, data) => {


  if (error) {
    return console.log(error);
  }

  let res = null;
  let lastIndex = 0;

  const newImportString = `import { ${controllerName} } from '../controllers/${slug}/controller';`;
  const newInjectString = `
  if (pageControllerName === '${slug}') {
    ${controllerName}();
  }
  `;

  let newData = data;

  // add the new import string at the end of the previous import
  const reg = /^(import)( +){.+}( +)(from)( +)'..{1,}';$/gm;

  while (res = reg.exec(newData)) {
    lastIndex = reg.lastIndex;
  }

  newData = newData.substr(0, lastIndex) + `\n${newImportString}` + newData.substr(lastIndex);

  // add new conditional for the new controller
  const regInjectStringLength = '// - end inject:js'.length;
  const regInject = /\/\/ - end inject:js/gm;

  while (res = regInject.exec(newData)) {
    lastIndex = regInject.lastIndex;
  }

  newData = newData.substr(0, lastIndex - regInjectStringLength) + `\n${newInjectString}` + newData.substr(lastIndex - regInjectStringLength);


  // copy the new string code to the filed
  fs.writeFile(config.path.js.controllerLoader, newData, 'utf8', (error) => {

    if (error) {
      return console.log(error);
    }

  });

});



console.log('==================');
console.log(`SUCCESS! "Controller ${controllerName}" has been created.`);
console.log('==================');
